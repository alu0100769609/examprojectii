﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
    public float speed = 20;
    public float turn_speed = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Vertical") != 0)
        {
            transform.Translate(Vector3.forward * speed * Input.GetAxis("Vertical") * 0.1F);
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            transform.Rotate(Vector3.up * turn_speed * Input.GetAxis("Horizontal"));
//            GetComponent<Rigidbody>().velocity = Vector3.right * Input.GetAxis("Horizontal") * velocidad;
        }
    }
}
