﻿using UnityEngine;
using System.Collections;

public class ChangeColorWhenSpace : MonoBehaviour {
    private int Uncolor = 0;
    // Use this for initialization
    void Start()
    {
        foreach (Renderer child in GetComponentsInChildren<Renderer>())
        {
            //            child.GetComponent<Renderer>().material.SetColor("_SpecColor", Color.red);
            child.material.color = Color.red;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (Renderer child in GetComponentsInChildren<Renderer>())
            {
                if (Uncolor == 0)
                {
                    child.material.color = Color.red;
                }
                if (Uncolor == 1)
                {
                    child.material.color = Color.yellow;
                }
                else if (Uncolor == 2)
                {
                    child.material.color = Color.green;
                }
            }
            if (Uncolor == 2)
                Uncolor = 0;
            else
                Uncolor++;
        }
    }
}
